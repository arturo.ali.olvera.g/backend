package com.example.backendtestapirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendTestApirestApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendTestApirestApplication.class, args);
    }

}
